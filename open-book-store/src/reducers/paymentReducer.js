import * as TYPE from "./../actions/action-types";
const initialState = { entities: [], loading: true, error: null };

const paymentReducer = (state = initialState, action) => {
  switch (action.type) {
    case TYPE.FETCH_PAYMENTDETAILS_BEGIN:
    case TYPE.ADD_PAYMENTDETAIL_BEGIN:
      return { ...state, loading: true, error: null };
    case TYPE.FETCH_PAYMENTDETAILS_SUCCESS:
      return { ...state, entities: action.payload, loading: false };
    case TYPE.FETCH_PAYMENTDETAILS_FAILURE:
      return { ...state, entities: [], loading: false, error: action.payload };
    case TYPE.ADD_PAYMENTDETAIL_SUCCESS:
      return { ...state, loading: false };
    case TYPE.ADD_PAYMENTDETAIL_FAILURE:
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};

export default paymentReducer;
