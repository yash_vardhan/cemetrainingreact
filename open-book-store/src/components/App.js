import React, { useEffect, useState, useCallback } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navigations from "./Navigations";
import PaymentForm from "./PaymentForm";
import PaymentList from "./PaymentList";

import { useDispatch } from "react-redux";
import { fetchPaymentDetails } from "../actions/paymentAction";

const App = () => {
  const dispatch = useDispatch();
  const [refresh, setRefresh] = useState(false);

  const PaymentDetails = useCallback(() => {
    dispatch(fetchPaymentDetails());
  }, [dispatch]);

  useEffect(() => {
   PaymentDetails();
  }, [refresh, PaymentDetails]);

  return (
    <Router>
      <React.Fragment>
      <Navigations/>
        <br />
        <Switch>
          <Route exact path="/">
            <PaymentList />
          </Route>
          <Route path="/add">
            <PaymentForm refresh={refresh} setRefresh={setRefresh} />
          </Route>
        </Switch>
      </React.Fragment>
    </Router>
  );
};

export default App;
