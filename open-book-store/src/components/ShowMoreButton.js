import React from "react";

const ShowMoreButton = (props) => (
  <button
    id="btnShowMore"
    className="btn btn-primary"
    onClick={() => props.toggle(!props.visible)}
  >
  </button>
);

export default ShowMoreButton;
