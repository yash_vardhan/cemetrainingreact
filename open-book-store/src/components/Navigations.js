import React from "react";
import { NavLink } from "react-router-dom";

const Navigation = () => (
  <nav className="navbar navbar-expand-lg navbar-primary">
    <div className="navbar-collapse">
      <ul className="navbar-nav mr-auto">
        <li className="nav-item">
          <NavLink exact className="nav-link" to="/">
            Payment List
          </NavLink>
        </li>
        <li className="nav-item">
          <NavLink exact className="nav-link" to="/add">
            Add Payment
          </NavLink>
        </li>
      </ul>
    </div>
  </nav>
);

export default Navigation;
