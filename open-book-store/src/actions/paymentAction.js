import axios from "axios";
import * as TYPE from "./action-types";

export const fetchPaymentDetailsBegin = () => {
  return {
    type: TYPE.FETCH_PAYMENTDETAILS_BEGIN,
  };
};

export const fetchPaymentDetailsSuccess = (payments) => {
  return {
    type: TYPE.FETCH_PAYMENTDETAILS_SUCCESS,
    payload: payments,
  };
};
export const fetchPaymentDetailsFailure = (err) => {
  return {
    type: TYPE.FETCH_PAYMENTDETAILS_FAILURE,
    payload: { message: "Failed to load details" },
  };
};

export const fetchPaymentDetails = () => {
  return (dispatch, getState) => {
    dispatch(fetchPaymentDetailsBegin);

    axios.get("http://localhost:8080/api/payment/all").then(
      (resp) => {
        dispatch(fetchPaymentDetailsSuccess(resp.data));
      },
      (err) => {
        dispatch(fetchPaymentDetailsFailure(err));
      }
    );
  };
};

export const addPayemntDetailBegin = () => {
  return {
    type: TYPE.ADD_PAYMENTDETAIL_BEGIN,
  };
};

export const addPayemntDetailsSuccess = () => {
  return {
    type: TYPE.ADD_PAYMENTDETAIL_SUCCESS,
  };
};

export const addPayemntDetailFailure = () => {
  return {
    type: TYPE.ADD_PAYMENTDETAIL_FAILURE,
    payload: {
      message: "Failed to add new payment detail.. please try again later",
    },
  };
};

export const addPaymentDetail = (payment) => {
  return (dispatch, getState) => {
    dispatch(addPayemntDetailBegin);
    return axios.post("http://localhost:8080/api/payment/save", payment).then(
      () => {
        alert("payment detail created!");
        dispatch(addPayemntDetailsSuccess());
        return true;
      },
      (err) => {
        dispatch(addPayemntDetailFailure(err));
        return false;
      }
    );
  };
};
